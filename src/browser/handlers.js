export async function message(event) {
    try {
        const ctx = this.createIncomingContext({
            app: this,
            websocket: event.target,
            message: event.data,
            event
        })

        return await this._onIncomingMessage.push(ctx)
    } catch (error) {
        this.emit('error', error)
    }
}

export function open(event) {
    this._websockets.add(event.target)
}

export function close(event) {
    this._websockets.delete(event.target)
}