import Client from '..'
import { open, close, message } from './handlers'

export default class BrowserClient extends Client {
    attach(url, protocols = []) {
        const ws = new WebSocket(url, protocols)
        
        ws.addEventListener('message', message.bind(this))
        ws.addEventListener('open', open.bind(this), { once: true })
        ws.addEventListener('close', close.bind(this), { once: true })

        return new Promise((resolve, reject) => {
            function done() {
                this.removeEventListener('close', fail)
                resolve()
            }

            function fail() {
                this.removeEventListener('open', done)
                reject()
            }

            ws.addEventListener('open', done, { once: true })
            ws.addEventListener('error', fail, { once: true })
        })
    }
}