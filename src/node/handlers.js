export async function message(websocket, message) {
    try {
        const ctx = this.createIncomingContext({
            app: this,
            websocket,
            message
        })

        return await this._onIncomingMessage.push(ctx)
    } catch (error) {
        this.emit('error', error)
    }
}

export function open(ws) {
    this._websockets.add(ws)
}

export function close(ws) {
    this._websockets.delete(ws)
}