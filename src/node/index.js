import Websocket from '@alexeimyshkouski/ws'

import Client from '..'
import { open, close } from './handlers'

export default class NodeClient extends Client {
    attach(url, options = {}) {
        const ws = new Websocket(url, options)

        ws
            .once('open', open.bind(this, ws))
            .once('close', close.bind(this, ws))
    }
}