import Node from '@alexeimyshkouski/realtime-core/node'
import { publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles'
import { BroadcastContext } from '@alexeimyshkouski/realtime-core/context'

@publisher @subscriber
class Client extends Node {
    _websockets = new Set()
    
    get connected() {
        return this._websockets
    }
}

Client.IncomingContext = BroadcastContext

export default Client