import path from 'path'

import external from 'rollup-external'
import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'

const __dist = path.resolve('dist')

const formats = new Map([
    ['cjs', {
        dir: __dist,
        entryFileNames: '[name].js',
        chunkFileNames: 'chunks/[hash].js',
        preferConst: true
    }],
    ['esm', {
        dir: __dist,
        entryFileNames: '[name].mjs',
        chunkFileNames: 'chunks/[hash].mjs',
        preferConst: true
    }],
    ['iife', {
        dir: __dist,
        entryFileNames: '[name].iife.js',
        name: 'RealtimeClient'
    }]
].map((entry) => {
    entry[1].format = entry[0]
    return entry
}))

export default [
    {
        input: 'src/node/index.js',
        output: [
            formats.get('esm'),
            formats.get('cjs')
        ],
        external: external(),
        plugins: [
            resolve({
                extensions: ['.mjs', '.js', '.json']
            }),
            babel({
                presets: [
                    ['@babel/preset-env', {
                        targets: ['maintained node versions'],
                        useBuiltIns: 'usage',
                        corejs: 3
                    }]
                ],
                plugins: [
                    ['@babel/plugin-proposal-decorators', { legacy: true }],
                    '@babel/plugin-proposal-class-properties'
                ]
            })
        ]
    },

    {
        input: {
            browser: 'src/browser/index.js'
        },
        output: formats.get('esm'),
        external: external(),
        plugins: [
            resolve(),
            babel({
                presets: [
                    ['@babel/preset-env', { targets: ['> 1%', 'not dead'] }]
                ],
                plugins: [
                    ['@babel/plugin-proposal-decorators', { legacy: true }],
                    '@babel/plugin-proposal-class-properties'
                ]
            })
        ]
    },

    {
        input: {
            browser: 'src/browser/index.js'
        },
        output: formats.get('iife'),
        plugins: [
            babel({
                runtimeHelpers: true,
                presets: [
                    ['@babel/preset-env', {
                        targets: ['> 1%', 'not dead']
                    }]
                ],
                plugins: [
                    ['@babel/plugin-proposal-decorators', { legacy: true }],
                    '@babel/plugin-proposal-class-properties',
                    ['@babel/plugin-transform-runtime', { regenerator: true }]
                ],

                babelrc: false,
                exclude: 'node_modules/@babel/**'
            }),
            resolve({
                preferBuiltins: false,
                mainFields: ['module', 'browser', 'main'],
                extensions: ['.mjs', '.js', '.json']
            }),
            commonjs()
        ]
    }
]