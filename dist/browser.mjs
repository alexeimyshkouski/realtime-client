import Node from '@alexeimyshkouski/realtime-core/node';
import { publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles';
import { BroadcastContext } from '@alexeimyshkouski/realtime-core/context';

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

var _class, _temp;

var Client = publisher(_class = subscriber(_class = (_temp =
/*#__PURE__*/
function (_Node) {
  _inherits(Client, _Node);

  function Client() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Client);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Client)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_websockets", new Set());

    return _this;
  }

  _createClass(Client, [{
    key: "connected",
    get: function get() {
      return this._websockets;
    }
  }]);

  return Client;
}(Node), _temp)) || _class) || _class;

Client.IncomingContext = BroadcastContext;

function message(_x) {
  return _message.apply(this, arguments);
}

function _message() {
  _message = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(event) {
    var ctx;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            ctx = this.createIncomingContext({
              app: this,
              websocket: event.target,
              message: event.data,
              event: event
            });
            _context.next = 4;
            return this._onIncomingMessage.push(ctx);

          case 4:
            return _context.abrupt("return", _context.sent);

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            this.emit('error', _context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 7]]);
  }));
  return _message.apply(this, arguments);
}

function open(event) {
  this._websockets.add(event.target);
}
function close(event) {
  this._websockets.delete(event.target);
}

var BrowserClient =
/*#__PURE__*/
function (_Client) {
  _inherits(BrowserClient, _Client);

  function BrowserClient() {
    _classCallCheck(this, BrowserClient);

    return _possibleConstructorReturn(this, _getPrototypeOf(BrowserClient).apply(this, arguments));
  }

  _createClass(BrowserClient, [{
    key: "attach",
    value: function attach(url) {
      var protocols = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      var ws = new WebSocket(url, protocols);
      ws.addEventListener('message', message.bind(this));
      ws.addEventListener('open', open.bind(this), {
        once: true
      });
      ws.addEventListener('close', close.bind(this), {
        once: true
      });
      return new Promise(function (resolve, reject) {
        function done() {
          this.removeEventListener('close', fail);
          resolve();
        }

        function fail() {
          this.removeEventListener('open', done);
          reject();
        }

        ws.addEventListener('open', done, {
          once: true
        });
        ws.addEventListener('error', fail, {
          once: true
        });
      });
    }
  }]);

  return BrowserClient;
}(Client);

export default BrowserClient;
