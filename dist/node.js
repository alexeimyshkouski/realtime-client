'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

const Websocket = _interopDefault(require('@alexeimyshkouski/ws'));
require('core-js/modules/es.array.iterator');
const Node = _interopDefault(require('@alexeimyshkouski/realtime-core/node'));
const roles = require('@alexeimyshkouski/realtime-core/roles');
const context = require('@alexeimyshkouski/realtime-core/context');
require('core-js/modules/es.promise');

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var _class, _temp;

let Client = roles.publisher(_class = roles.subscriber(_class = (_temp = class Client extends Node {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "_websockets", new Set());
  }

  get connected() {
    return this._websockets;
  }

}, _temp)) || _class) || _class;

Client.IncomingContext = context.BroadcastContext;

function open(ws) {
  this._websockets.add(ws);
}
function close(ws) {
  this._websockets.delete(ws);
}

class NodeClient extends Client {
  attach(url, options = {}) {
    const ws = new Websocket(url, options);
    ws.once('open', open.bind(this, ws)).once('close', close.bind(this, ws));
  }

}

module.exports = NodeClient;
