var RealtimeClient = (function (_classCallCheck, _createClass, _possibleConstructorReturn, _getPrototypeOf, _inherits, _assertThisInitialized, _defineProperty, Node, roles, context, _regeneratorRuntime, _asyncToGenerator) {
    'use strict';

    _classCallCheck = _classCallCheck && _classCallCheck.hasOwnProperty('default') ? _classCallCheck['default'] : _classCallCheck;
    _createClass = _createClass && _createClass.hasOwnProperty('default') ? _createClass['default'] : _createClass;
    _possibleConstructorReturn = _possibleConstructorReturn && _possibleConstructorReturn.hasOwnProperty('default') ? _possibleConstructorReturn['default'] : _possibleConstructorReturn;
    _getPrototypeOf = _getPrototypeOf && _getPrototypeOf.hasOwnProperty('default') ? _getPrototypeOf['default'] : _getPrototypeOf;
    _inherits = _inherits && _inherits.hasOwnProperty('default') ? _inherits['default'] : _inherits;
    _assertThisInitialized = _assertThisInitialized && _assertThisInitialized.hasOwnProperty('default') ? _assertThisInitialized['default'] : _assertThisInitialized;
    _defineProperty = _defineProperty && _defineProperty.hasOwnProperty('default') ? _defineProperty['default'] : _defineProperty;
    Node = Node && Node.hasOwnProperty('default') ? Node['default'] : Node;
    _regeneratorRuntime = _regeneratorRuntime && _regeneratorRuntime.hasOwnProperty('default') ? _regeneratorRuntime['default'] : _regeneratorRuntime;
    _asyncToGenerator = _asyncToGenerator && _asyncToGenerator.hasOwnProperty('default') ? _asyncToGenerator['default'] : _asyncToGenerator;

    var _class, _temp;

    var Client = roles.publisher(_class = roles.subscriber(_class = (_temp =
    /*#__PURE__*/
    function (_Node) {
      _inherits(Client, _Node);

      function Client() {
        var _getPrototypeOf2;

        var _this;

        _classCallCheck(this, Client);

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Client)).call.apply(_getPrototypeOf2, [this].concat(args)));

        _defineProperty(_assertThisInitialized(_this), "_websockets", new Set());

        return _this;
      }

      _createClass(Client, [{
        key: "connected",
        get: function get() {
          return this._websockets;
        }
      }]);

      return Client;
    }(Node), _temp)) || _class) || _class;

    Client.IncomingContext = context.BroadcastContext;

    function message(_x) {
      return _message.apply(this, arguments);
    }

    function _message() {
      _message = _asyncToGenerator(
      /*#__PURE__*/
      _regeneratorRuntime.mark(function _callee(event) {
        var ctx;
        return _regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                ctx = this.createIncomingContext({
                  app: this,
                  websocket: event.target,
                  message: event.data,
                  event: event
                });
                _context.next = 4;
                return this._onIncomingMessage.push(ctx);

              case 4:
                return _context.abrupt("return", _context.sent);

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                this.emit('error', _context.t0);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7]]);
      }));
      return _message.apply(this, arguments);
    }

    function open(event) {
      this._websockets.add(event.target);
    }
    function close(event) {
      this._websockets.delete(event.target);
    }

    var BrowserClient =
    /*#__PURE__*/
    function (_Client) {
      _inherits(BrowserClient, _Client);

      function BrowserClient() {
        _classCallCheck(this, BrowserClient);

        return _possibleConstructorReturn(this, _getPrototypeOf(BrowserClient).apply(this, arguments));
      }

      _createClass(BrowserClient, [{
        key: "attach",
        value: function attach(url) {
          var protocols = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
          var ws = new WebSocket(url, protocols);
          ws.addEventListener('message', message.bind(this));
          ws.addEventListener('open', open.bind(this), {
            once: true
          });
          ws.addEventListener('close', close.bind(this), {
            once: true
          });
          return new Promise(function (resolve, reject) {
            function done() {
              this.removeEventListener('close', fail);
              resolve();
            }

            function fail() {
              this.removeEventListener('open', done);
              reject();
            }

            ws.addEventListener('open', done, {
              once: true
            });
            ws.addEventListener('error', fail, {
              once: true
            });
          });
        }
      }]);

      return BrowserClient;
    }(Client);

    return BrowserClient;

}(_classCallCheck, _createClass, _possibleConstructorReturn, _getPrototypeOf, _inherits, _assertThisInitialized, _defineProperty, Node, roles, context, _regeneratorRuntime, _asyncToGenerator));
