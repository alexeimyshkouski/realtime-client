import Websocket from '@alexeimyshkouski/ws';
import 'core-js/modules/es.array.iterator';
import Node from '@alexeimyshkouski/realtime-core/node';
import { publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles';
import { BroadcastContext } from '@alexeimyshkouski/realtime-core/context';
import 'core-js/modules/es.promise';

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var _class, _temp;

let Client = publisher(_class = subscriber(_class = (_temp = class Client extends Node {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "_websockets", new Set());
  }

  get connected() {
    return this._websockets;
  }

}, _temp)) || _class) || _class;

Client.IncomingContext = BroadcastContext;

function open(ws) {
  this._websockets.add(ws);
}
function close(ws) {
  this._websockets.delete(ws);
}

class NodeClient extends Client {
  attach(url, options = {}) {
    const ws = new Websocket(url, options);
    ws.once('open', open.bind(this, ws)).once('close', close.bind(this, ws));
  }

}

export default NodeClient;
